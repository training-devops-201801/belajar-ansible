# Belajar Ansible #

## Instalasi Ansible ##

* Ubuntu

    ```
    $ sudo apt-get update
    $ sudo apt-get install software-properties-common
    $ sudo apt-add-repository ppa:ansible/ansible
    $ sudo apt-get update
    $ sudo apt-get install ansible
    ```

## Inventory File ##

Adalah file yang berisi daftar host dengan grupnya.

```
[aplikasi-web]
belajar-devops01.artivisi.id
belajar-devops02.artivisi.id
belajar-devops03.artivisi.id
belajar-devops04.artivisi.id
belajar-devops05.artivisi.id

[database-server]
db01.artivisi.id
db02.artivisi.id
```

## Tasks ##

Daftar perintah yang ingin dijalankan di host

```yml
---
- hosts: aplikasi-belajar
  become: yes
  become_user: root
  tasks:
   - name: Install OpenJDK 8
     apt:
       name: openjdk-8-jdk-headless
       state: present
   - name: Install Haveged
     apt:
       name: haveged
       state: present
```

## Run ##

```
ansible-playbook -i hosts jdk8.yml --user root
```

## Referensi ##

* https://github.com/arbabnazar/ansible-roles
* https://leucos.github.io/ansible-files-layout
* https://wiredcraft.com/blog/getting-started-with-ansible-in-5-minutes
* https://serversforhackers.com/c/an-ansible2-tutorial
* https://www.redhat.com/en/blog/system-administrators-guide-getting-started-ansible-fast